# Crossref Internal SDK

This is library is a collection of classes used internally by Crossref services.

Classes: 

* [CrossrefAuthorizationInterface](./README.md#crossrefauthorizationinterface)

## CrossrefAuthorizationInterface

A simple library that encapsulates HTTP requests to keycloak in order to verify permissions given an API key, saving the toil of handling api keys (salt and hash)

The way it works is shown in the following schema: 

```plantuml
@startuml
start

:user http request received;
partition "Crossref Auth Interface" {
:obtain apikey from headers;

:Add salt to key and encode it with sha256;

:Communicate the API key to keycloak + resource info + client secret;
}
if (boolean returned) then (true)

  :grant access to resource;
else (false)
  :deny access to resource;
endif

stop
@enduml
```



## Getting started

Here is some kotlin code trying to access, we assume there is a HttpRequest-like object accessible, where a user has asked to access some resource.

```kotlin
// val request = HttpServletRequest( .... 

val cai = CrossrefAuthorizationInterface("MyClient", "client-secret", "keycloakRealm", "https://kmeycloak-server/auth/")

if (cai.checkPermissions ("resource-name", request)) {
	return resourceVar;
} else {
	throw NotAuthorizedException()
}
```

If the API key has been obtained in a different way not included in the standard headers of the request, or the request object is not HttpServletRequest, you can extract it and provide it manually
```kotlin
// val request = HttpServletRequest( .... 
// val apiKey = request.headers["crossref-api-key"]

val cai = CrossrefAuthorizationInterface("MyClient", "client-secret", "keycloakRealm", "https://kmeycloak-server/auth/")

if (cai.checkPermissions ("resource-name", apiKey)) {
	return resourceVar;
} else {
	throw NotAuthorizedException()
}
```

