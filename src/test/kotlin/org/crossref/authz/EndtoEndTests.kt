package org.crossref.authz

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation
import org.junit.jupiter.api.Order
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.TestMethodOrder
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient
import java.io.ByteArrayInputStream
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URI
import java.nio.charset.StandardCharsets
import javax.servlet.http.HttpServletRequest
import kotlin.random.Random
import kotlin.test.assertEquals
import kotlin.test.assertTrue
import java.net.URI as URIJavaNet

@TestMethodOrder(OrderAnnotation::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class EndtoEndTests {

    val REALM = System.getenv("CROSSREF_REALM") ?: "crossref"
    val KEYCLOAK_URL = URI(System.getenv("KEYCLOAK_URL") ?: "http://localhost:8888/")

    val cw =
        CloudWatchClient.builder().endpointOverride(
            URIJavaNet(System.getenv("LOCALSTACK_URL") ?: "http://localhost:4566"),
        ).region(Region.US_EAST_1).credentialsProvider(EnvironmentVariableCredentialsProvider.create()).build()

    val cai = CrossrefAuthorizationInterface(
        "crossref-resources",
        "**********",
        "",
        REALM,
        KEYCLOAK_URL.toString(),
        AWSApiMetrics(
            "test_service",
            1,
            cw,
        ),
    )

    var stafftoken: String? = null
    var usertoken: String? = null
    var memberId: Int? = null
    var testUserId: String? = null
    var apiKey: String? = null
    var keyId: String? = null

    val AUTH_URL = URI(
        KEYCLOAK_URL.scheme,
        null,
        KEYCLOAK_URL.host,
        KEYCLOAK_URL.port,
        "/auth/realms/$REALM/protocol/openid-connect/token",
        null,
        null,
    ).toString()

    val MANAGEMENT_URL = URI(
        KEYCLOAK_URL.scheme,
        null,
        KEYCLOAK_URL.host,
        KEYCLOAK_URL.port,
        "/auth/realms/$REALM/crossref-management",
        null,
        null,
    ).toString()

    val mapper = ObjectMapper()

    private fun callKeycloakEndpoint(endpoint: String, token: String?, body: String?, method: String = "GET", contentType: String = "application/json") = doReq(
        MANAGEMENT_URL + endpoint,
        token,
        body,
        method,
        contentType,
    )

    private fun doReq(url: String, auth: String?, body: String?, method: String = "GET", contentType: String = "application/json"): Pair<Int, String> {
        val connection = URI(url).toURL().openConnection() as HttpURLConnection
        connection.requestMethod = method
        connection.setRequestProperty("Authorization", auth)
        body?.let {
            connection.doOutput = true
            connection.setRequestProperty("Content-Type", contentType)
            with(body.toByteArray(StandardCharsets.UTF_8)) {
                connection.setRequestProperty("Content-Length", this.size.toString())
                DataOutputStream(connection.outputStream).let {
                    it.write(this)
                    it.flush()
                    it.close()
                }
            }
        }

        val stringBuilder = StringBuilder()
        val reader = InputStreamReader((if (connection.responseCode in 200..399) connection.inputStream else connection.errorStream) ?: ByteArrayInputStream(byteArrayOf()), StandardCharsets.UTF_8)

        reader.use {
            reader.readLines().forEach { line ->
                stringBuilder.append(line)
            }
        }
        connection.disconnect()

        return Pair(connection.responseCode, stringBuilder.toString())
    }

    fun login(username: String, pwd: String): String {
        val (_, body) = doReq(
            AUTH_URL,
            null,
            mapOf(
                "client_id" to "ui",
                "username" to username,
                "grant_type" to "password",
                "password" to pwd,
                "scope" to "openid",
            ).map { "${it.key}=${it.value}" }.joinToString("&"),
            "POST",
            "application/x-www-form-urlencoded",
        )
        return mapper.readValue<Map<String, String>>(body)["access_token"] ?: throw Exception("login failed")
    }

    @BeforeAll
    fun before_or_after_all() {
        stafftoken = "Bearer " + login("test@crossref.org", "pwdPWD1!")
        usertoken = "Bearer " + login("test@user.com", "pwdPWD1!")

        callKeycloakEndpoint(
            "/organization",
            stafftoken,
            mapper.writeValueAsString(
                mapOf(
                    "orgId" to "e2971d12-145a-4fae-a1da-969c456819c6",
                    "name" to "psychoceramics-${Random.nextInt(0,10000)}",
                    "memberId" to Random.nextInt(1000, 10000),
                ),
            ),
            "POST",
        ).let { (status, body) ->
            assertEquals(201, status)
            memberId = mapper.readValue<Map<String, Any>>(body)["memberId"] as Int
        }

        callKeycloakEndpoint(
            "/user",
            stafftoken,
            null,
            "GET",
        ).let { (status, body) ->
            assertEquals(200, status)
            testUserId = mapper.readValue<List<Map<String, String>>>(body).filter { it["username"] == "test@user.com" }.first()["userId"]
        }

        callKeycloakEndpoint(
            "/organization/$memberId/user/$testUserId",
            stafftoken,
            null,
            "POST",
        ).let { (status, _) ->
            assertEquals(204, status)
        }

        callKeycloakEndpoint(
            "/organization/$memberId/key",
            usertoken,
            mapper.writeValueAsString(
                mapOf(
                    "keyId" to "e2971d12-145a-4fae-a1da-969c456819c6",
                    "issuedBy" to "e2971d12-145a-4fae-a1da-969c456819c6",
                    "name" to "name",
                    "description" to "description",
                ),
            ),
            "POST",
        ).let { (status, body) ->
            assertEquals(201, status)
            mapper.readValue<Map<String, Any>>(body).let {
                keyId = (it["keyObject"] as Map<*, *>)["keyId"] as String?
                apiKey = it["key"] as String
            }
        }
    }

    @Test
    @Order(1)
    fun `Accessing rest api with test user fails`() {
        assertEquals(false, cai.checkPermission("plus-data", apiKey!!).first)
        assertEquals(
            false,
            cai.checkPermission(
                "plus-data",
                mockk<HttpServletRequest> {
                    every { getHeader("authorization") } returns "Bearer " + apiKey
                    every { getHeader("crossref-api-key") } returns null
                    every { getHeader("crossref-plus-api-token") } returns null
                    every { servletPath } returns "/"
                },
            ).first,
        )
        assertEquals(
            false,
            cai.checkPermission(
                "plus-data",
                mockk<HttpServletRequest> {
                    every { getHeader("authorization") } returns null
                    every { getHeader("crossref-api-key") } returns apiKey
                    every { getHeader("crossref-plus-api-token") } returns null
                    every { servletPath } returns "/"
                },
            ).first,
        )
        assertEquals(
            false,
            cai.checkPermission(
                "plus-data",
                mockk<HttpServletRequest> {
                    every { getHeader("authorization") } returns null
                    every { getHeader("crossref-api-key") } returns null
                    every { getHeader("crossref-plus-api-token") } returns "Bearer " + apiKey
                    every { servletPath } returns "/"
                },
            ).first,
        )
    }

    @Test
    @Order(2)
    fun `Accessing rest api with test user works`() {
        callKeycloakEndpoint(
            "/organization/$memberId/subscription/METADATA_PLUS",
            stafftoken,
            null,
            "PUT",
        ).let { (status, _) ->
            assertEquals(204, status)
        }

        Thread.sleep(3000)

        assertEquals(true, cai.checkPermission("metadata-plus-data", apiKey!!).first)
        assertEquals(
            true,
            cai.checkPermission(
                "metadata-plus-data",
                mockk<HttpServletRequest> {
                    every { getHeader("authorization") } returns "Bearer " + apiKey
                    every { getHeader("crossref-api-key") } returns null
                    every { getHeader("crossref-plus-api-token") } returns null
                    every { servletPath } returns "/"
                },
            ).first,
        )
        assertEquals(
            true,
            cai.checkPermission(
                "metadata-plus-data",
                mockk<HttpServletRequest> {
                    every { getHeader("authorization") } returns null
                    every { getHeader("crossref-api-key") } returns apiKey
                    every { getHeader("crossref-plus-api-token") } returns null
                    every { servletPath } returns "/"
                },
            ).first,
        )
        assertEquals(
            true,
            cai.checkPermission(
                "metadata-plus-data",
                mockk<HttpServletRequest> {
                    every { getHeader("authorization") } returns null
                    every { getHeader("crossref-api-key") } returns null
                    every { getHeader("crossref-plus-api-token") } returns "Bearer " + apiKey
                    every { servletPath } returns "/"
                },
            ).first,
        )
    }

    @Test
    @Order(3)
    fun `Accessing rest api permission matches subscription`() {
        Thread.sleep(2000)
        assertEquals(true, cai.checkPermission("metadata-plus-data", apiKey!!).first)

        callKeycloakEndpoint(
            "/organization/$memberId/subscription/METADATA_PLUS",
            stafftoken,
            null,
            "DELETE",
        ).let { (status, _) ->
            assertEquals(204, status)
        }

        Thread.sleep(2000)

        assertEquals(false, cai.checkPermission("metadata-plus-data", apiKey!!).first)

        callKeycloakEndpoint(
            "/organization/$memberId/subscription/METADATA_PLUS",
            stafftoken,
            null,
            "PUT",
        ).let { (status, _) ->
            assertEquals(204, status)
        }

        Thread.sleep(2000)
        assertEquals(true, cai.checkPermission("metadata-plus-data", apiKey!!).first)
    }

    @Test
    @Order(4)
    fun `Metrics are sent`() {
        cw.listMetrics().metrics().let { metrics ->
            assertTrue { metrics.isNotEmpty() }
            assertTrue { metrics.filter { it.namespace().equals("ECS/application-metrics") }.isNotEmpty() }
        }
    }
}
