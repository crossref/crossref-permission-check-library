package org.crossref.authz

import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import org.junit.jupiter.api.Test
import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider
import software.amazon.awssdk.regions.Region
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient
import java.net.URI
import javax.servlet.http.HttpServletRequest

internal class CrossrefAuthorizationInterfaceTest {
    private val permissionChecker = CrossrefAuthorizationInterface(
        "rest-api",
        "supersecret",
        "",
        "crossref",
        System.getenv("KEYCLOAK_URL") ?: "http://docker.local:8888/",
        AWSApiMetrics(
            "test_service",
            1,
            CloudWatchClient.builder().endpointOverride(
                URI(System.getenv("LOCALSTACK_URL") ?: "http://docker.local:4566"),
            ).region(Region.US_EAST_1).credentialsProvider(EnvironmentVariableCredentialsProvider.create()).build(),
        ),

    )

    private val pc = spyk(permissionChecker) {
        every { checkPermission("resource", "goodkey", "/", "scope").first } returns true
        every { checkPermission("resource", "badkey", "/", "scope").first } returns false
    }

    @Test
    fun `Bad key in authorization header fails`() {
        assert(
            !pc.checkPermission(
                "resource",
                mockk<HttpServletRequest> {
                    every { getHeader("authorization") } returns "Bearer badkey"
                    every { getHeader("crossref-api-key") } returns null
                    every { getHeader("crossref-plus-api-token") } returns null
                    every { servletPath } returns "/"
                },
                "scope",
            ).first,
        )
    }

    @Test
    fun `Good key in authorization header fails`() {
        assert(
            pc.checkPermission(
                "resource",
                mockk<HttpServletRequest> {
                    every { getHeader("authorization") } returns "Bearer goodkey"
                    every { getHeader("crossref-api-key") } returns null
                    every { getHeader("crossref-plus-api-token") } returns null
                    every { servletPath } returns "/"
                },
                "scope",
            ).first,
        )
    }

    @Test
    fun `api token header has priority over authorization`() {
        assert(
            pc.checkPermission(
                "resource",
                mockk<HttpServletRequest> {
                    every { getHeader("authorization") } returns "Bearer badkey"
                    every { getHeader("crossref-plus-api-token") } returns "Bearer goodkey"
                    every { getHeader("crossref-api-key") } returns null
                    every { servletPath } returns "/"
                },
                "scope",
            ).first,
        )
    }

    @Test
    fun `crossref-api-key has priority over everything`() {
        assert(
            pc.checkPermission(
                "resource",
                mockk<HttpServletRequest> {
                    every { getHeader("authorization") } returns "Bearer badkey"
                    every { getHeader("crossref-plus-api-token") } returns "Bearer badkey"
                    every { getHeader("crossref-api-key") } returns "goodkey"
                    every { servletPath } returns "/"
                },
                "scope",
            ).first,
        )
    }

    @Test
    fun `crossref-api-key on its own works`() {
        assert(
            pc.checkPermission(
                "resource",
                mockk<HttpServletRequest> {
                    every { getHeader("authorization") } returns null
                    every { getHeader("crossref-plus-api-token") } returns null
                    every { getHeader("crossref-api-key") } returns "goodkey"
                    every { servletPath } returns "/"
                },
                "scope",
            ).first,
        )
    }
}
