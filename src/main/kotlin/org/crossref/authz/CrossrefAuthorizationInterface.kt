package org.crossref.authz

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.google.common.cache.CacheBuilder
import com.google.common.cache.CacheLoader
import com.google.common.cache.LoadingCache
import com.google.common.hash.Hashing
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import software.amazon.awssdk.services.cloudwatch.CloudWatchClient
import software.amazon.awssdk.services.cloudwatch.model.Dimension
import software.amazon.awssdk.services.cloudwatch.model.MetricDatum
import software.amazon.awssdk.services.cloudwatch.model.PutMetricDataRequest
import software.amazon.awssdk.services.cloudwatch.model.StandardUnit
import java.io.ByteArrayInputStream
import java.io.DataOutputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URI
import java.nio.charset.StandardCharsets
import java.time.Instant
import java.util.concurrent.TimeUnit
import javax.servlet.http.HttpServletRequest

class ApiKeyCheckingException : Exception {

    constructor(e: Exception, body: String, headers: MutableMap<String, MutableList<String>>) : super(
        ObjectMapper().writeValueAsString(
            mapOf(
                "exceptionMessage" to e.message,
                "exceptionName" to e.javaClass.name,
                "responseHeaders" to headers.toString(),
                "responseBody" to body,
            ),
        ),
    )
}
interface MetricPublisher {
    suspend fun addSuccess(keyName: String)
    suspend fun addFailure(keyName: String)
}

class NoApiMetrics : MetricPublisher {
    override suspend fun addFailure(keyName: String) {}

    override suspend fun addSuccess(keyName: String) {}
}

class AWSApiMetrics(val service: String, val metricsDelay: Int = 300, val cwClient: CloudWatchClient? = null) : MetricPublisher {
    /*
     * This library tries to catch all exceptions nand prints errors on the STDERR output.
     * As metrics are not critical, errors should not disrupt the service using them.
     */
    private val data = mutableMapOf<String, IntArray>()
    private val mutex = Mutex()
    val cw = getCloudWatchClient()
    var lastSendingTime = Instant.now().epochSecond

    private fun getCloudWatchClient(): CloudWatchClient? {
        return cwClient ?: try { CloudWatchClient.builder().build() } catch (e: Exception) {
            System.err.println("[ERROR] crossref-internal-sdk: Couldn't conect to Cloudwatch")
            System.err.println(e)
            return null
        }
    }

    companion object {
        const val KEY_NAME_DIMENSION_NAME = "KEY_NAME"
        const val METRIC_NAMESPACE = "ECS/application-metrics"
    }

    override suspend fun addSuccess(keyName: String) {
        mutex.withLock {
            (data.getOrPut(keyName) { intArrayOf(0, 0) })[0]++
        }
        sendMetrics()
    }

    override suspend fun addFailure(keyName: String) {
        mutex.withLock {
            (data.getOrPut(keyName) { intArrayOf(0, 0) })[1]++
        }
        sendMetrics()
    }

    private suspend fun sendMetrics() {
        val datums: MutableList<MetricDatum> = mutableListOf()
        val curTime = Instant.now().epochSecond
        if ((curTime - metricsDelay) >= lastSendingTime) {
            mutex.withLock {
                try {
                    if ((curTime - metricsDelay) >= lastSendingTime) {
                        lastSendingTime = curTime

                        val timestamp = Instant.now()

                        datums.addAll(
                            (
                                data.map { (keyName, data) ->
                                    MetricDatum.builder()
                                        .metricName("apikey-access-$service-success-auth")
                                        .timestamp(timestamp)
                                        .unit(StandardUnit.COUNT)
                                        .value(data[0].toDouble())
                                        .dimensions(
                                            Dimension.builder().name(KEY_NAME_DIMENSION_NAME)
                                                .value(keyName).build(),
                                        ).build()
                                } + data.map { (keyName, data) ->
                                    MetricDatum.builder()
                                        .metricName("apikey-access-$service-failed-auth")
                                        .timestamp(timestamp)
                                        .value(data[1].toDouble())
                                        .dimensions(
                                            Dimension.builder().name(KEY_NAME_DIMENSION_NAME)
                                                .value(keyName).build(),
                                        ).build()
                                }
                                ).filter { it.value() > 0 },
                        )
                        data.clear()
                    }
                } catch (e: Exception) {
                    System.err.println("[ERROR] crossref-internal-sdk: Couldn't create DATUM package")
                }
            }

            if (datums.isNotEmpty()) {
                try {
                    cw?.putMetricData(
                        PutMetricDataRequest.builder()
                            .namespace(METRIC_NAMESPACE)
                            .metricData(datums).build(),
                    )
                } catch (e: Exception) {
                    System.err.println("[ERROR] crossref-internal-sdk: Couldn't send datums to CloudWatch")
                    System.err.println(e)
                }
            }
        }
    }
}

class CrossrefAuthorizationInterface(private val clientName: String, private val clientSecret: String, private val apiKeySalt: String, realm: String, keycloakUrl: String, apiMetrics: MetricPublisher) {
    private val bearerAccessTokenPattern = "^Bearer (.+)$".toPattern()

    private val mapper = ObjectMapper()
    private val metrics = apiMetrics

    data class CacheKey(val resource: String, val apiKey: String, val path: String?, val scope: String?)

    private var authCache: LoadingCache<CacheKey, Pair<Boolean, Map<String, Any>>>? = null

    companion object {
        const val MAX_RETRIES = 3
        const val RETRY_WAIT_MS: Long = 2000
    }

    fun setCache(size: Long, seconds: Long) {
        authCache = CacheBuilder.newBuilder()
            .maximumSize(size)
            .expireAfterWrite(seconds, TimeUnit.SECONDS)
            .build(
                object : CacheLoader<CacheKey, Pair<Boolean, Map<String, Any>>>() {
                    override fun load(key: CacheKey): Pair<Boolean, Map<String, Any>> {
                        return executeCheckPermission(key.resource, key.apiKey, key.path, key.scope)
                    }
                },
            )
    }

    private val permissionUrl = URI(keycloakUrl).let { originalUri ->
        URI(
            originalUri.scheme,
            null,
            originalUri.host,
            originalUri.port,
            "/auth/realms/$realm/checkApiKeyPermissions",
            null,
            null,
        ).toURL()
    }

    private fun hashApiKey(key: String) = Hashing.sha256()
        .hashString(apiKeySalt + key, Charsets.US_ASCII)
        .toString()
        .lowercase()

    private fun getLegacyToken(req: HttpServletRequest): String? {
        val token1 = req.getHeader("crossref-plus-api-token") ?: ""
        val token2 = req.getHeader("authorization") ?: ""

        val matcher1 = bearerAccessTokenPattern.matcher(token1)
        val matcher2 = bearerAccessTokenPattern.matcher(token2)
        return if (matcher1.matches()) {
            matcher1.group(1)
        } else if (matcher2.matches()) {
            matcher2.group(1)
        } else { null }
    }

    fun doJsonPost(body: Map<String, String>): Triple<Int, String, MutableMap<String, MutableList<String>>> {
        val connection = permissionUrl.openConnection() as HttpURLConnection
        connection.requestMethod = "POST"
        connection.doOutput = true
        connection.setRequestProperty("Content-Type", "application/json; utf-8")
        with(mapper.writeValueAsString(body).toByteArray(StandardCharsets.UTF_8)) {
            connection.setRequestProperty("Content-Length", this.size.toString())
            DataOutputStream(connection.outputStream).let {
                it.write(this)
                it.flush()
                it.close()
            }
        }

        val stringBuilder = StringBuilder()
        val reader = InputStreamReader((if (connection.responseCode in 200..399) connection.inputStream else connection.errorStream) ?: ByteArrayInputStream(byteArrayOf()), StandardCharsets.UTF_8)

        reader.use {
            reader.readLines().forEach { line ->
                stringBuilder.append(line)
            }
        }
        connection.disconnect()

        return Triple(connection.responseCode, stringBuilder.toString(), connection.headerFields)
    }

    fun checkPermission(resource: String, apiKey: String, path: String? = null, scope: String? = null): Pair<Boolean, Map<String, Any>> =
        (authCache?.get(CacheKey(resource, apiKey, path, scope)) ?: executeCheckPermission(resource, apiKey, path, scope)).also {
                (success, data) ->
            data["keyName"]?.let {
                runBlocking {
                    launch {
                        try {
                            it as String
                            if (it.isNotEmpty()) {
                                if (success) metrics.addSuccess(it) else metrics.addFailure(it)
                            }
                        } catch (e: Exception) {
                            // To integrate with logging, this project is being used by Clojure and java codebases,
                            // we should investigate how to integrate all the logging
                            System.err.println("[ERROR] crossref-internal-sdk: Cannot store metrics")
                        }
                    }
                }
            }
        }

    fun executeCheckPermission(resource: String, apiKey: String, path: String? = null, scope: String? = null): Pair<Boolean, Map<String, Any>> {
        val body = mapOf(
            "apiKeyHash" to hashApiKey(apiKey),
            "resource" to resource,
            "clientName" to clientName,
            "clientSecret" to clientSecret,
            "scope" to (scope ?: ""),
            "resourcePath" to (path ?: ""),
        )

        var responseBody = ""
        var responseHeaders: MutableMap<String, MutableList<String>> = mutableMapOf()
        var responseCode = 0

        try {
            doJsonPost(body).let { (status, response, headers) ->
                responseBody = response
                responseHeaders = headers
                responseCode = status
            }
            return Pair(responseCode == 200, mapper.readValue(responseBody))
        } catch (e: Exception) {
            throw ApiKeyCheckingException(e, responseBody, responseHeaders)
        }
    }

    fun reqContainsApiKey(req: HttpServletRequest) = req.getHeader("crossref-api-key") ?: getLegacyToken(req) != null

    fun checkPermission(resource: String, req: HttpServletRequest, scope: String? = null): Pair<Boolean, Map<String, Any>> {
        val apikey = req.getHeader("crossref-api-key") ?: getLegacyToken(req) ?: return Pair(false, mapOf())

        return checkPermission(resource, apikey, req.servletPath, scope)
    }
}
